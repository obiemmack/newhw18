﻿#include <iostream>
#include <string>
using namespace std;

template <typename T>
class Stack 
{
private:
    int count = 0;
    int size;
    T* arr = new T[size];
    
public:
    Stack (int StackSize)
    {
        size = StackSize;
        T* newarr = new T[size];
        delete[] arr;
        arr = newarr;
    }

    void push(T value)
    {
        if (count < size)
        {
            arr[count] = value;
            cout << "Pushed " << value << endl;
            count++;
        }
        else
        {
            cout << "Stack is full" << endl;
        }
    }

    void pop()
    {
        if (count > 0)
        {
            count--;
            cout << arr[count] << " removed" <<endl;
            
        }
        else
        {
            cout << "Stack is empty" << endl;
        }
    }
    void show()
    {
        if (count > 0)
        {
            for (int i = 0; i < count; i++)
            {
                std::cout << arr[i] << "\t";
            }
            cout << endl<<endl;
        }
        else
        {
            cout << "Stack is empty" << endl;
        }
    }


};

int main()
{
    Stack<int> i (4);  

    i.push(1);
    i.push(2);
    i.push(3);
    i.push(4);
    i.push(5);
    i.show();
    i.pop();
    i.pop();
    i.pop();
    i.pop();
    i.pop();
    cout << endl << endl;

    
    Stack<double> d(4);

    d.push(11.11);
    d.push(22.22);
    d.push(33.33);
    d.push(44.44);
    d.push(44.44);
    d.show();
    d.pop();
    d.pop();
    d.pop();
    d.pop();
    d.pop();
    cout << endl << endl;

    Stack<std::string> s(3);

    s.push("Hello ");
    s.push("World ");
    s.push("!!!");
    s.push("blablabla");
    s.show();
    s.pop();
    s.pop();
    s.pop();
    s.pop();
    cout << endl << endl;
        

}